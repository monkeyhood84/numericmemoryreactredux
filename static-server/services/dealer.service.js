var pairsList = require('../data/pairs.js');

function shuffleArray (o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function shuffleCards() {
    var cards = [];
    for (var i = 0; i < (pairsList.length); i++) {
        var card = JSON.parse(JSON.stringify(pairsList[i]));
        card.id = i + 1;
        card.visible = false;
        card.matched = false;
        cards.push(card);
        var pairedCard = JSON.parse(JSON.stringify(card));
        //this way of clone might not work if the object contains functions
        pairedCard.id = card.id + pairsList.length;
        cards.push(pairedCard);;
    }
    return shuffleArray(cards);
}

module.exports = {
    shuffleCards: shuffleCards
}
//export default shuffleCards;

