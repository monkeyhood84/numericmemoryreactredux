var express = require('express');
var app = express();
var path = require('path');
var dealer = require('./services/dealer.service.js');

//app.use(express.static('static'))

app.use(function(req, res, next) {
  // res.setHeader("Access-Control-Allow-Origin", "*");
    //res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    //res.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type");
  next();
});

app.get('/api/newgame', function (req, res, next) {    
    var newGame = dealer.shuffleCards();
    console.log(newGame);
    res.send(newGame);
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("App listening at http://%s:%s", host, port)
})