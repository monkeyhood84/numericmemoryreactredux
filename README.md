# Numeric Memory

A simple app(game) to put in practice the concepts learnt about React & Redux. Store, Middleware, Immutable, Combine Reducer, Hiearchy Reducers, Action Creation, Async Actions, Chamaleon Actions, Connect, Containers, Presentation Components...

It was a good opportunity to add Linting(ESLint) and Unit Testing (Jest & Enzyme) to the app.

It also has been used to separate the create game to an API using express.
http://localhost:8081/api/newgame
That endpoint could be change to any other API which would return the same template. But for simplicy put it in the same project.


More detailed below.

## Getting Started

These instructions will let you know the main commands available.

### Installing

By default it will install the required modules for the app and for the API.

```
npm install
```

To install individually

**App**
```
npm run install-app
```
or
```
cd app
npm install
```

**API**
```
npm run install-server
```
or
```
cd static-server
npm install
```

### Build App

```
npm run build
```
or
```
npm run build-app
```
or
```
cd app
npm run build
```

### Running

By default it will launch the app and the server.

```
npm start
```

To run it individually

**App**

It also includes a build.

```
npm run start-app
```
or
```
cd app
npm start
```

**API**
```
npm run start-server
```
or
```
cd static-server
npm start
```

### Running the tests

```
npm test
```
or
```
npm run test-app
```
or
```
cd app
npm test
```

### Linting check

```
npm run lint
```
or
```
npm run lint-app
```
or
```
cd app
npm run lint
```

## Technologies

* ES6
* React
* Redux
* ImmutableJS
* Babel
* Webpack
* Express
* ESLint
* Jest
* Enzyme

## Numeric Memory React Redux

![Memory App Redux](extras/Memory-React-Redux-Graph.png?raw=true "Memory App Redux")

### Store

The store handles a small state which is composed members of different types (Array of Objects, Objects and Int). All objects are cards� so strongly type it to a class or interface would not be totally crazy but for that small app knowing the flow of the data is not needed.

The initial state is:
```json
{
	listOfCards: [],
	firstCard: null,
	secondCard: null,
	numOfTries: 0
}
```

Also this app uses *null* as a value of part of the state... although that afects more to the reducers.

The only **Middleware** applied is *Immutable*. *Trunk* or *Saga* are not used as there is only one scenario where it would useful and that scenario is quite simple and with a simple fetch can archive the same. (For future update)

### Actions

The User Interactions Actions are only 2.

The first action, *newGame*, is an **Async Action**. As it is the only one, the action creator will take the dispatch as a parameter to perform the dispatch when the API service finishes. If the request fails it return a mocked game.

The other action, *turnCardUp* is a **Chameleon Action**. The action creator with its params determines which has to be actual **Action Creator**. The options are *firstCardUp*, *ignoreCard*, *cardsMatched*, *cardNotMatched*. All are quite self explanatories.

### Reducers

Two options for the reducers has been considered.

First a initial reducer which handles the state in each action and only delegates to an auxiliary reducer for the cards inside the array.

As this is app is for practice and to try ways that are more extensible and scalable, I used a second approach where each state property has its own reducer. Cards reducer still delegates to card reducer. First and Second card reducers are different because they react different for the same actions.

![Memory App Components](extras/Memory-React-Redux-Component.png?raw=true "Memory App Components")

### Components

For the component structure there are 6 components which 5 of those have a container to allow those components to access directly to a part of the store and to the required actions.

Only *Card* is a pure **Presentational Component**. The others uses **Connect** and there are grouped in 3 types of **Containers**:
* Only Read part of the Store: *Status, Stats*
* Only Uses Actions: *App, Options*
* Reads Store and *Use Actions: Board*

The last one also uses the **merge** parameter of Connect, to bind the dispatch action.
