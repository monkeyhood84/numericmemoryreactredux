
export const newCard = () => { 
    return { 
        id: 0,
        text: '',
        matched: false
    };
};

export const areSameCard = (card1, card2) => (card1 && card2 && (card1.id === card2.id)) ? true : false;

export const areMatching = (card1, card2) => (card1 && card2 && (card1.text === card2.text)) ? true : false;

export const numOfPairs = (cards) => (cards && (cards.length > 1)) 
                                                ? (cards.length / 2) : 0;

export const isMatched = card => ((card||false) && card.matched);
export const isNotMatched = card => !(isMatched(card));

export const numOfMatchedPairs = (cards) => (cards && cards.length) 
                                                ? (cards.filter(isMatched).length / 2) : 0;

export const getNotMatchedCards = cards => cards ? cards.filter(isNotMatched) : [];