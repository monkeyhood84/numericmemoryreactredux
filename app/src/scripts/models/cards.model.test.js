import React from 'react';
import jest from 'jest-mock';
import Enzyme, { shallow, mount } from 'enzyme';
import * as cardUtils from './cards.model';
import expect from 'expect';

describe('Cards Model (Utils) ', () => {    

    describe('areSameCard', () => {
        it('2 undefined cards', () => {
            const res = cardUtils.areSameCard();
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first undefined second defined', () => {
            const res = cardUtils.areSameCard(null, {id:4});
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first defined, second undefined', () => {
            const res = cardUtils.areSameCard({id:4}, null);
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first and second are the same', () => {
            const res = cardUtils.areSameCard({id:4}, {id:4});
            expect(res).toBe(true);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first and second are differnt', () => {
            const res = cardUtils.areSameCard({id:2}, {id:4});
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });
    });

    describe('areMatching', () => {
        it('2 undefined cards', () => {
            const res = cardUtils.areMatching();
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first undefined second defined', () => {
            const res = cardUtils.areMatching(null, {text:'4'});
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first defined, second undefined', () => {
            const res = cardUtils.areMatching({text:'4'}, null);
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first and second match', () => {
            const res = cardUtils.areMatching({text:'4'}, {text:'4'});
            expect(res).toBe(true);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });

        it('first and second not match', () => {
            const res = cardUtils.areMatching({text:'2'}, {text:'4'});
            expect(res).toBe(false);
            expect(res).not.toBe(null);
            expect(res).not.toBe(undefined);
        });
    });

    describe('numOfPairs', () => {
        it('undefined list cards', () => {
            const res = cardUtils.numOfPairs();
            expect(res).toBe(0);
        });

        it('empty list card', () => {
            const res = cardUtils.numOfPairs([]);
            expect(res).toBe(0);
        });

        it('list with cards', () => {
            const res = cardUtils.numOfPairs(['a','b','c','d']);
            expect(res).toBe(2);
        });
    });

    describe('isMatched', () => {
        it('undefined card', () => {
            const res = cardUtils.isMatched();
            expect(res).toBe(false);
        });

        it('not matched card', () => {
            const res = cardUtils.isMatched({matched:false});
            expect(res).toBe(false);
        });

        it('matched card', () => {
            const res = cardUtils.isMatched({matched:true});
            expect(res).toBe(true);
        });
    });

    describe('isNotMatched', () => {
        it('undefined card', () => {
            const res = cardUtils.isNotMatched();
            expect(res).toBe(true);
        });

        it('not matched card', () => {
            const res = cardUtils.isNotMatched({matched:false});
            expect(res).toBe(true);
        });

        it('matched card', () => {
            const res = cardUtils.isNotMatched({matched:true});
            expect(res).toBe(false);
        });
    });

    describe('numOfMatchedPairs', () => {
        it('undefined list cards', () => {
            const res = cardUtils.numOfMatchedPairs();
            expect(res).toBe(0);
        });

        it('empty list card', () => {
            const res = cardUtils.numOfMatchedPairs([]);
            expect(res).toBe(0);
        });

        it('list with cards', () => {
            const res = cardUtils.numOfMatchedPairs([{matched:true},{matched:false},{matched:false},{matched:true}]);
            expect(res).toBe(1);
        });
    });

    describe('getNotMatchedCards', () => {
        it('undefined list cards', () => {
            const res = cardUtils.getNotMatchedCards();
            expect(res.length).toBe(0);
        });

        it('empty list card', () => {
            const res = cardUtils.getNotMatchedCards([]);
            expect(res.length).toBe(0);
        });

        it('list with cards', () => {
            const res = cardUtils.getNotMatchedCards([{matched:true},{matched:false},{matched:false},{matched:true}]);
            expect(res.length).toBe(2);
        });
    });

});