
import mockedGame from '../data/mockedGame';

const getMockedGame = () => {
    return mockedGame;
};

const getNewGame = () => {
    return fetch('api/newgame',
        {
            cache: 'no-cache',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'content-type': 'application/json'
            },
            method: 'GET',
        })
        .then((response) => {
            return response.json();
        })
        .catch(err => {
            /* eslint-disable-next-line no-console */
            console.error('Failed to connect to the server. Loading default game.', err);
            return getMockedGame();
        });
};

export default {
    getNewGame: getNewGame,
    getMockedGame: getMockedGame
};