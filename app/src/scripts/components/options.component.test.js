import React from 'react';
import jest from 'jest-mock';
import Enzyme, { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ConnectedOptionsComponent, { OptionsComponent } from './options.component';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-15';
import expect from 'expect';

Enzyme.configure({ adapter: new Adapter() });

describe('Options Component ', () => {
    describe('Presentation Component', () => {
        let wrapper;
        const mockFunction = jest.fn();
        const props = {
            resetGame: mockFunction
        };

        beforeEach(() => {
            wrapper = shallow(<OptionsComponent {...props} />);
            expect(wrapper).not.toBe(null);
        });

        it('button is displayed', () => {
            expect(wrapper.find('button')).not.toBe(null);
        });

        it('button has been clicked', () => {
            const refreshButton = wrapper.find('button');
            refreshButton.simulate('click');

            expect(mockFunction.mock.calls.length).toBe(1);
        });

    });



});