import React, { PropTypes } from "react";
import ReactDOM from "react-dom";

export default class CardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.onSelectedCard = this.onSelectedCard.bind(this);
  }

  isMatched() {
    return this.props.card.matched;
  }

  isSelected() {
    return this.props.isSelected;
  }

  isVisible() {
    return (this.props.isSelected || this.isMatched());
  }

  onSelectedCard() {
    if (!this.isVisible() && this.props.onSelectedCard) {
      this.props.onSelectedCard(this.props.card);
    }
  }

  render() {
    const card = this.props.card;
    const isMatched = this.isMatched();
    const isSelected = this.isSelected();
    const isVisible = this.isVisible();

    const cardContent = isVisible ? <span className="card-content">{card.text}</span> : '';
    const cardClasses = "card"
      + (isSelected ? " card-selected" : '')
      + (isMatched ? " card-visible" : '');

    return (
      <div className="col-xs-4 col-sm-3 col-md-2">
        <div className={cardClasses}
          onClick={this.onSelectedCard}>
          {cardContent}
        </div>
      </div>
    );
  }
}

CardComponent.propTypes = {
  card: PropTypes.object.isRequired,
  isSelected: PropTypes.bool.isRequired,
  onSelectedCard: PropTypes.func.isRequired
};