import React, { PropTypes } from "react";
import ReactDOM from "react-dom";
import {connect} from 'react-redux';
import * as cardUtils from '../models/cards.model';

export class StatusComponent extends React.Component {
  render() {
    const numOfPairs = cardUtils.numOfPairs(this.props.game);
    if (!(numOfPairs > 0 && cardUtils.numOfMatchedPairs(this.props.game) === numOfPairs)) {
      return null;
    }
    const percentage = numOfPairs / this.props.numOfTries;
    const roundPercentage = Math.round(percentage * 100, 2);
    return (      
      <div className="status-box">
        <h2>Well Done!</h2>
        <p>You have completed it with the { roundPercentage }% of guesses</p>
      </div>
    );
  }
}

StatusComponent.propTypes = {
  game: PropTypes.array.isRequired,
  numOfTries: PropTypes.number.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    game: state.listOfCards,
    numOfTries: state.numOfTries
  };
}

export default connect(mapStateToProps, null)(StatusComponent);