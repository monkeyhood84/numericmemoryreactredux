import React from 'react';
import jest from 'jest-mock';
import Enzyme, { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import CardComponent from './card.component';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-15';
import expect from 'expect';

Enzyme.configure({ adapter: new Adapter() });

describe('Card Component ', () => {
    
    describe('Presentation Component (Turned Down)', () => {
        let wrapper;
        const mockFunction = jest.fn();
        const props = {
            card: { matched: false },
            isSelected: false,
            onSelectedCard: mockFunction
        };

        beforeEach(() => {
            wrapper = shallow(<CardComponent {...props} />);
            expect(wrapper).not.toBe(null);
        });

        it('Content not revealed', () => {
            expect(wrapper.find('div.card').children().length).toBe(0);
            expect(wrapper.find('div.card-selected').isEmpty()).toBe(true);
            expect(wrapper.find('div.card-visible').isEmpty()).toBe(true);
        });

        it('card can be clicked', () => {
            const card = wrapper.find('div.card');
            card.simulate('click');
            expect(mockFunction.mock.calls.length).toBe(1);
        });

    });

    describe('Presentation Component (Selected)', () => {
        let wrapper;
        const mockFunction = jest.fn();
        const props = {
            card: { matched: false },
            isSelected: true,
            onSelectedCard: mockFunction
        };

        beforeEach(() => {
            wrapper = shallow(<CardComponent {...props} />);
            expect(wrapper).not.toBe(null);
        });

        it('Content revealed', () => {
            expect(wrapper.find('div.card').children().length).toBe(1);
            expect(wrapper.find('div.card-selected').isEmpty()).toBe(false);
            expect(wrapper.find('div.card-visible').isEmpty()).toBe(true);
        });

        it('card can NOT clicked', () => {
            const card = wrapper.find('div.card');
            card.simulate('click');
            expect(mockFunction.mock.calls.length).toBe(0);
        });

    });


    describe('Presentation Component (Matched)', () => {
        let wrapper;
        const mockFunction = jest.fn();
        const props = {
            card: { matched: true },
            isSelected: false,
            onSelectedCard: mockFunction
        };

        beforeEach(() => {
            wrapper = shallow(<CardComponent {...props} />);
            expect(wrapper).not.toBe(null);
        });

        it('Content revealed', () => {
            expect(wrapper.find('div.card').children().length).toBe(1);
            expect(wrapper.find('div.card-selected').isEmpty()).toBe(true);
            expect(wrapper.find('div.card-visible').isEmpty()).toBe(false);
        });

        it('card can NOT be clicked', () => {
            const card = wrapper.find('div.card');
            card.simulate('click');
            expect(mockFunction.mock.calls.length).toBe(0);
        });

    });

});