import React from 'react';
import jest from 'jest-mock';
import Enzyme, { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ConnectedBoardComponent, { BoardComponent } from './board.component';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-15';
import expect from 'expect';

Enzyme.configure({ adapter: new Adapter() });

describe('Board Component ', () => {
    const mockFunction = jest.fn();

    describe('Presentation Component (Game Not Initialzed)', () => {
        it('displays an empty board', () => {
            const props = {
                game: [],
                firstCard: null,
                secondCard: null,
                turnCardUp: mockFunction
            };
            let wrapper = mount(<BoardComponent {...props} />);
            expect(wrapper).not.toBe(null);
            expect(wrapper.find('div.card').length).toBe(0);
        });
    });

    describe('Presentation Component (Game Started/Completed)', () => {
        it('dislays the board with all the cards', () => {
            const props = {
                game: [
                    { id: 1, matched: false },
                    { id: 2, matched: false },
                    { id: 3, matched: false },
                    { id: 4, matched: false }
                ],
                firstCard: { id: 3, matched: false },
                secondCard: null,
                turnCardUp: mockFunction
            };
            let wrapper = mount(<BoardComponent {...props} />);
            expect(wrapper).not.toBe(null);
            expect(wrapper.find('div.card').length).toBe(4);
        });
    });
});