import React from 'react';
import jest from 'jest-mock';
import Enzyme, { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ConnectedNumMemoryApp, { NumMemoryApp } from './app.component';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-15';
import expect from 'expect';

Enzyme.configure({ adapter: new Adapter() });

describe('App Component ', () => {    

    describe('Presentation Component', () => {
        it('load and call for a new game', () => {
            const mockFunction = jest.fn();
            const props = {
                resetGame: mockFunction
            };
            const wrapper = shallow(<NumMemoryApp {...props} />);
            expect(wrapper).not.toBe(null);
            expect(wrapper.find('h1').text()).toBe('Numeric Memory');
            expect(mockFunction.mock.calls.length).toBe(1);
        });
    });
});