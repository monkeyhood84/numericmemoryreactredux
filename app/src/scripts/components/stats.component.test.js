import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ConnectedStatsComponent, { StatsComponent } from './stats.component';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-15';
import expect from 'expect';

Enzyme.configure({ adapter: new Adapter() });

describe('Stats Component ', () => {

    describe('Presentation Component (Game Not Loaded)', () => {
        it('no pairs no matches', () => {
            const props = {
                game: []
            };
            let wrapper = shallow(<StatsComponent {...props} />);
            expect(wrapper).not.toBe(null);

            expect(wrapper.find('meter').prop('value')).toBe(0);
            expect(wrapper.find('meter').prop('max')).toBe(0);
        });
    });
    
    describe('Presentation Component (Game Not Started)', () => {
        it('with pairs but no matches', () => {
            const props = {
                game: [{ matched: false }, { matched: false }, { matched: false }, { matched: false }]
            };
            let wrapper = shallow(<StatsComponent {...props} />);
            expect(wrapper).not.toBe(null);

            expect(wrapper.find('meter').prop('value')).toBe(0);
            expect(wrapper.find('meter').prop('max')).toBe(2);
        });
    });

    describe('Presentation Component (Game in Progress)', () => {
        it('with pairs but not all matches', () => {
            const props = {
                game: [{ matched: false }, { matched: true }, { matched: true }, { matched: false }]
            };
            let wrapper = shallow(<StatsComponent {...props} />);
            expect(wrapper).not.toBe(null);

            expect(wrapper.find('meter').prop('value')).toBe(1);
            expect(wrapper.find('meter').prop('max')).toBe(2);
        });
    });

    describe('Presentation Component (Game Compleeted)', () => {
        it('with pairs but not all matches', () => {
            const props = {
                game: [{ matched: true }, { matched: true }, { matched: true }, { matched: true }]
            };
            let wrapper = shallow(<StatsComponent {...props} />);
            expect(wrapper).not.toBe(null);

            expect(wrapper.find('meter').prop('value')).toBe(2);
            expect(wrapper.find('meter').prop('max')).toBe(2);
        });
    });



});