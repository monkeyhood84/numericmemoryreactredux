import React, { PropTypes } from "react";
import ReactDOM from "react-dom";

/*  eslint-disable import/no-named-as-default */
import StatsComponent from './stats.component';
import OptionsComponent from './options.component';
import BoardComponent from './board.component';
import StatusComponent from './status.component';
/*  eslint-enable import/no-named-as-default */

import {connect} from 'react-redux';
import * as gameActions from './../actions/game.actions';

export class NumMemoryApp extends React.Component {
  componentWillMount() {
    this.props.resetGame();
  }

  render() {
    return (
      <div>
        <header className="page-header row">
          <h1>Numeric Memory</h1>
        </header>
        <div id="game-extras" className="row">
          <StatsComponent/>
          <OptionsComponent />
        </div>
        <StatusComponent/>
        <BoardComponent />
      </div>
    );
  }
}

NumMemoryApp.propTypes = {
  resetGame: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    resetGame: () => gameActions.resetGame(dispatch)
  };
}

export default connect(null, mapDispatchToProps)(NumMemoryApp);