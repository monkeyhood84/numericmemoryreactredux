import React, { PropTypes } from "react";
import ReactDOM from "react-dom";
import {connect} from 'react-redux';
import API from '../services/api.service';
import * as gameActions from './../actions/game.actions';

export class OptionsComponent extends React.Component {
  render() {
    return (
      <aside className="col-xs-4 text-right game-options">
        <button className="btn btn-xs btn-success" onClick={this.props.resetGame}>
          <span className="glyphicon glyphicon-refresh" aria-hidden="true" />
          RESTART
        </button>
      </aside>
    );
  }
}

OptionsComponent.propTypes = {
  resetGame: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    resetGame: () => gameActions.resetGame(dispatch)
  };
}

export default connect(null, mapDispatchToProps)(OptionsComponent);