import React, { PropTypes } from "react";
import ReactDOM from "react-dom";
import CardComponent from './card.component';
import { connect } from 'react-redux';
import * as gameActions from './../actions/game.actions';
import * as cardUtils from '../models/cards.model';

export class BoardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.renderCard = this.renderCard.bind(this);
  }

  renderCard(card) {
    const isSelected = (cardUtils.areSameCard(card, this.props.firstCard)
      || cardUtils.areSameCard(card, this.props.secondCard));
    return (<CardComponent
      id={card.id}
      key={card.id}
      card={card}
      isSelected={isSelected}
      onSelectedCard={this.props.turnCardUp} />);
  }

  render() {
    const board = this;
    const cards = this.props.game.map(this.renderCard);
    return (
      <section id="game-board" className="row">
        {cards}
      </section>
    );
  }
}

BoardComponent.propTypes = {
  game: PropTypes.array.isRequired,
  firstCard: PropTypes.object,
  secondCard: PropTypes.object,
  turnCardUp: PropTypes.func.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    game: state.listOfCards,
    firstCard: state.firstCard,
    secondCard: state.secondCard
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    turnCardUp: (card, first, second) => dispatch(gameActions.turnCardUp(card, first, second))
  };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    turnCardUp: (card) => dispatchProps.turnCardUp(card, stateProps.firstCard, stateProps.secondCard)
  };
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(BoardComponent);