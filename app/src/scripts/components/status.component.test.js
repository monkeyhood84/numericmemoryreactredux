import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ConnectedStatusComponent, { StatusComponent } from './status.component';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-15';
import expect from 'expect';

Enzyme.configure({ adapter: new Adapter() });

describe('Status Component ', () => {
    // it('+++capturing Snapshot of StatusComponent', () => {
    //     const renderedValue = renderer.create(<StatusComponent game={[]} numOfTries={0} />).toJSON();
    //     expect(renderedValue).toMatchSnapshot();
    // });

    describe('Presentation Component (Game Not Initialzed)', () => {
        it('Component is not displayed', () => {
            const props = {
                game: [],
                numOfTries: 0
            };
            let wrapper = shallow(<StatusComponent {...props} />);
            expect(wrapper.text()).toBe('');
        });
    });

    describe('Presentation Component (Game Not Started)', () => {
        it('Component is not displayed', () => {
            const props = {
                game: [],
                numOfTries: 0
            };
            let wrapper = shallow(<StatusComponent {...props} />);
            expect(wrapper.text()).toBe('');
        });
    });

    describe('Presentation Component (Game in progress)', () => {
        it('Component is not displayed', () => {
            const props = {
                game: [{ matched: false }, { matched: false }, { matched: false }, { matched: false }],
                numOfTries: 0
            };
            let wrapper = shallow(<StatusComponent {...props} />);
            expect(wrapper.text()).toBe('');
        });
    });

    describe('Presentation Component (Game Completed)', () => {
        let wrapper;
        const props = {
            game: [{ matched: true }, { matched: true }],
            numOfTries: 5
        };

        beforeEach(() => {
            wrapper = shallow(<StatusComponent {...props} />);
            expect(wrapper).not.toBe(null);
        });

        it('h2 is shown as expected', () => {
            expect(wrapper.contains(<h2>Well Done!</h2>)).toBe(true);
            expect(wrapper.find('h2').text()).toBe('Well Done!');
        });

        it('p has the right percentage', () => {
            expect(wrapper.find('p').text()).toContain('20%');
            expect(wrapper.find('p').text()).not.toContain('40%');
        });

    });



});