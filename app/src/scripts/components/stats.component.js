import React, { PropTypes } from "react";
import ReactDOM from "react-dom";
import { connect } from 'react-redux';
import * as cardUtils from '../models/cards.model';

export class StatsComponent extends React.Component {
  render() {
    let totalPairs = cardUtils.numOfPairs(this.props.game);
    let matchedPairs = cardUtils.numOfMatchedPairs(this.props.game);
    return (
      <section className="col-xs-8 text-left game-stats">
        <b>Matched Pairs</b>
        <meter value={matchedPairs} min="0" max={totalPairs} />
        {matchedPairs}/{totalPairs}
      </section>
    );
  }
}

StatsComponent.propTypes = {
  game: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  return {
    game: state.listOfCards
  };
}

export default connect(mapStateToProps, null)(StatsComponent);