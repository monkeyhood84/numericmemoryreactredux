require('./../assets/styles.scss');
import React from "react";
import ReactDOM from "react-dom";
import {createStore} from "redux";
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import NumMemoryApp from './components/app.component';

const initialState = {
    listOfCards: [],
    firstCard: null,
    secondCard: null,
    numOfTries: 0
}

const store = configureStore(initialState);
ReactDOM.render(
    (<Provider store={store}>
        <NumMemoryApp />
    </Provider>),
    document.getElementById('root')
);

