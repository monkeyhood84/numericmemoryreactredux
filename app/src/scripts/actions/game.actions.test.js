import * as types from './actionTypes';
import * as actions from './game.actions';
import expect from 'expect';

describe('Actions ', () => {    

    describe('turnCardUp', () => {
        it('first card is defined but second card is not defined', () => {
            const res = actions.turnCardUp();
            expect(res.type).toBe(types.FIRST_CARD_UP);
        });

        it('first card and card are the same', () => {
            const res = actions.turnCardUp({id:5, text:'a'}, {id:5, text:'b'});
            expect(res.type).toBe(types.IGNORE_CARD);
        });

        it('first card and card are different but have the same value', () => {
            const res = actions.turnCardUp({id:5, text:'a'}, {id:6, text:'a'});
            expect(res.type).toBe(types.CARDS_MATCHED);
        });

        it('first card and card are different and have different value', () => {
            const res = actions.turnCardUp({id:5, text:'a'}, {id:6, text:'b'});
            expect(res.type).toBe(types.CARDS_NOT_MATCHED);
        });
    });       

});