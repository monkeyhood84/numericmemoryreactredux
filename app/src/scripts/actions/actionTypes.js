export const NEW_GAME = 'NEW_GAME';
export const IGNORE_CARD = 'IGNORE_CARD';
export const FIRST_CARD_UP = 'FIRST_CARD_UP';
export const CARDS_MATCHED = 'CARDS_MATCHED';
export const CARDS_NOT_MATCHED = 'CARDS_NOT_MATCHED';
