import * as types from './actionTypes';
import * as cardUtils from '../models/cards.model';
import API from '../services/api.service';

export function resetGame(dispatch) {
    return API.getNewGame()
        .then(data => dispatch({
            type: types.NEW_GAME,
            data: data
        }));
}

export function ignoreCard() {
    return {
        type: types.IGNORE_CARD
    };
}

export function firstCardUp(card) {
    return {
        type: types.FIRST_CARD_UP,
        data: card
    };
}

export function cardsMatched(firstCard, secondCard) {
    return {
        type: types.CARDS_MATCHED,
        data: {
            firstCard: firstCard,
            secondCard: secondCard
        }
    };
}

export function cardsNotMatched(card) {
    return {
        type: types.CARDS_NOT_MATCHED,
        data: card
    };
}


export function turnCardUp(card, firstCard, secondCard) {
    if (!firstCard || secondCard) {
        return firstCardUp(card);
    }
    else if (cardUtils.areSameCard(firstCard, card)) {
        return ignoreCard();
    }
    else if (cardUtils.areMatching(firstCard, card)) {
        return cardsMatched(firstCard, card);
    }
    else {
        return cardsNotMatched(card);
    }
}