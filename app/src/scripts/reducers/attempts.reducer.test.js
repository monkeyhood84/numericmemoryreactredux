import attemptsReducer from './attempts.reducer';
import * as types from '../actions/actionTypes';
import * as actions from '../actions/game.actions';
import expect from 'expect';

describe('Reducer:: Attempts ', () => {    

    describe('NEW_GAME', () => {
        const action = {
                type: types.NEW_GAME,
                data: []
            };
        let initialState;

        afterEach(() => {
            const newState = attemptsReducer(initialState, action);
            expect(newState).toBe(0);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game was just created', () => {
            initialState = 0;
        });

        it('when game has started/completed', () => {
            initialState = 4;
        });
    });

    describe('CARDS_MATCHED', () => {
        const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {},
                    secondCard: {}
                }
            };
        let initialState;

        afterEach(() => {
            const newState = attemptsReducer(initialState, action);
            expect(newState).toBe(initialState + 1);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game was just created', () => {
            initialState = 0;
        });

        it('when game has started/completed', () => {
            initialState = 4;
        });
    });       

    describe('CARDS_NOT_MATCHED', () => {
        const action = {
                type: types.CARDS_NOT_MATCHED,
                data: {}
            };
        let initialState;

        afterEach(() => {
            const newState = attemptsReducer(initialState, action);
            expect(newState).toBe(initialState + 1);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game was just created', () => {
            initialState = 0;
        });

        it('when game has started/completed', () => {
            initialState = 4;
        });
    });    
    
    describe('FIRST_CARD_UP', () => {
        const action = {
                type: types.FIRST_CARD_UP,
                data: {}
            };
        let initialState;

        afterEach(() => {
            const newState = attemptsReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game was just created', () => {
            initialState = 0;
        });

        it('when game has started/completed', () => {
            initialState = 4;
        });
    });    
       
    describe('IGNORE_CARD', () => {
        const action = {
                type: types.IGNORE_CARD
            };
        let initialState;

        afterEach(() => {
            const newState = attemptsReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game was just created', () => {
            initialState = 0;
        });

        it('when game has started/completed', () => {
            initialState = 4;
        });
    }); 

});