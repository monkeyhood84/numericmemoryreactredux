import cardReducer from './card.reducer';
import * as types from '../actions/actionTypes';
import * as actions from '../actions/game.actions';
import expect from 'expect';

describe('Reducer:: Card ', () => {    

    describe('CARDS_MATCHED', () => {
        const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {},
                    secondCard: {}
                }
            };
        let initialState;

        afterEach(() => {
            
        });

        it('card is not initialized', () => {
            const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {},
                    secondCard: {}
                }
            };
            const initialState = undefined;
            const newState = cardReducer(initialState, action);
            expect(newState.id).toBe(undefined);
            expect(newState.matched).toBe(undefined);
        });

        it('card is not first or second card', () => {
            const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {id:1},
                    secondCard: {id:2}
                }
            };
            const initialState = {id:3};
            const newState = cardReducer(initialState, action);
            expect(newState.id).toBe(initialState.id);
        });

        it('card is firstCard', () => {
            const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {id:1, matched: false},
                    secondCard: {id:2, matched: false}
                }
            };
            const initialState = {id:1, matched: false};
            const newState = cardReducer(initialState, action);
            expect(newState.id).toBe(initialState.id);
            expect(newState.matched).toBe(true);
        });

        it('card is second', () => {
            const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {id:1, matched: false},
                    secondCard: {id:2, matched: false}
                }
            };
            const initialState = {id:2, matched: false};
            const newState = cardReducer(initialState, action);
            expect(newState.id).toBe(initialState.id);
            expect(newState.matched).toBe(true);
        });
    });

});