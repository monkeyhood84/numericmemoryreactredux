import * as types from '../actions/actionTypes';

const firstCardReducer = (state = null, action) => {
    switch (action.type) {
        case types.NEW_GAME:
        case types.CARDS_MATCHED:
            return null;
        case types.FIRST_CARD_UP:
            return action.data;
        case types.CARDS_NOT_MATCHED:        
        case types.IGNORE_CARD:
        default:
            return state;
    }
};

export default firstCardReducer;