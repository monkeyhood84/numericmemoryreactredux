import firstCardReducer from './firstCard.reducer';
import * as types from '../actions/actionTypes';
import * as actions from '../actions/game.actions';
import expect from 'expect';

describe('Reducer:: firstCard ', () => {    

    describe('NEW_GAME', () => {
        const action = {
                type: types.NEW_GAME,
                data: []
            };
        let initialState;

        afterEach(() => {
            const newState = firstCardReducer(initialState, action);
            expect(newState).toBe(null);
        });

        it('when game was not initialized/just started', () => {
            initialState = null;
        });

        it('when game has started/completed', () => {
            initialState = {id:4};
        });
    });

    describe('CARDS_MATCHED', () => {
        const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {},
                    secondCard: {}
                }
            };
        let initialState;

        afterEach(() => {
            const newState = firstCardReducer(initialState, action);
            expect(newState).toBe(null);
        });

        it('when game was not initialized/just started', () => {
            initialState = null;
        });

        it('when game has started/completed', () => {
            initialState = {id:4};
        });
    });       

    describe('CARDS_NOT_MATCHED', () => {
        const action = {
                type: types.CARDS_NOT_MATCHED,
                data: {id:7}
            };
        let initialState;

        afterEach(() => {
            const newState = firstCardReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized/just started', () => {
            initialState = null;
        });

        it('when game has started/completed', () => {
            initialState = {id:4};
        });
    });    
    
    describe('FIRST_CARD_UP', () => {
        const action = {
                type: types.FIRST_CARD_UP,
                data: {id:7}
            };
        let initialState;

        afterEach(() => {
            const newState = firstCardReducer(initialState, action);
            expect(newState.id).toBe(action.data.id);
        });

        it('when game was not initialized/just started', () => {
            initialState = null;
        });

        it('when game has started/completed', () => {
            initialState = {id:4};
        });
    });    
       
    describe('IGNORE_CARD', () => {
        const action = {
                type: types.IGNORE_CARD
            };
        let initialState;

        afterEach(() => {
            const newState = firstCardReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized/just started', () => {
            initialState = null;
        });

        it('when game has started/completed', () => {
            initialState = {id:4};
        });
    }); 

});