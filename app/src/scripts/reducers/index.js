import { combineReducers } from 'redux';
import cards from './cards.reducer';
import firstCard from './firstCard.reducer';
import secondCard from './secondCard.reducer';
import attempts from './attempts.reducer';

const rootReducer = combineReducers({
    listOfCards: cards,
    firstCard: firstCard,
    secondCard: secondCard,
    numOfTries: attempts
});

export default rootReducer;