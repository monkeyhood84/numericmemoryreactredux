import * as types from '../actions/actionTypes';

const attemptsReducer = (state = 0, action) => {
    switch (action.type) {
        case types.NEW_GAME:
            return 0;
        case types.CARDS_MATCHED:
        case types.CARDS_NOT_MATCHED:
            return state + 1;
        case types.FIRST_CARD_UP:
        case types.IGNORE_CARD:
        default:
            return state;
    }
};

export default attemptsReducer;