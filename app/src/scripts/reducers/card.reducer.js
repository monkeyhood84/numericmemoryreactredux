import * as types from '../actions/actionTypes';
import * as cardUtils from '../models/cards.model';

const cardReducer = (state = {}, action) => {
    switch (action.type) {
        case types.CARDS_MATCHED:
            if( state && state.id && 
            ( cardUtils.areSameCard(state, action.data.firstCard)
            || cardUtils.areSameCard(state, action.data.secondCard))) {
                return {...state, matched: true};
            }
            return state;
        default:
            return state;
    }
};

export default cardReducer;