import * as types from '../actions/actionTypes';
import * as cardUtils from '../models/cards.model';
import cardReducer from './card.reducer';

const cardsReducer = (state = [], action) => {
    switch (action.type) {
        case types.NEW_GAME:
            return action.data;
        case types.CARDS_MATCHED:
            return state.map(card => cardReducer(card, action));
        default:
            return state;
    }
};

export default cardsReducer;