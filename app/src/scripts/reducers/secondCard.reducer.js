import * as types from '../actions/actionTypes';

const secondCardReducer = (state = null, action) => {
    switch (action.type) {
        case types.NEW_GAME:
        case types.FIRST_CARD_UP:
        case types.CARDS_MATCHED:           
            return null;
        case types.CARDS_NOT_MATCHED:
            return action.data;                
        case types.IGNORE_CARD:
        default:
            return state;
    }
};

export default secondCardReducer;