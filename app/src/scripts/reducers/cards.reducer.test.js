import cardsReducer from './cards.reducer';
import * as types from '../actions/actionTypes';
import * as actions from '../actions/game.actions';
import expect from 'expect';

describe('Reducer:: firstCard ', () => {    

    describe('NEW_GAME', () => {
        const action = {
                type: types.NEW_GAME,
                data: [{id:9}, {id:8}]
            };
        let initialState;

        afterEach(() => {
            const newState = cardsReducer(initialState, action);
            expect(newState.length).toBe(action.data.length);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game empty', () => {
            initialState = [];
        });

        it('when game has started/completed', () => {
            initialState = [{id:1}, {id:2}, {id:3}, {id:4}];
        });
    });

    describe('CARDS_MATCHED', () => {
        const action = {
                type: types.CARDS_MATCHED,
                data: {
                    firstCard: {},
                    secondCard: {}
                }
            };
        let initialState;

        afterEach(() => {
            const newState = cardsReducer(initialState, action);
            expect(newState.length).toBe((initialState||[]).length);
        });

        it('when game was not initialized', () => {
            initialState = undefined;
        });

        it('when game empty', () => {
            initialState = [];
        });

        it('when game has started/completed', () => {
             initialState = [{id:1}, {id:2}, {id:3}, {id:4}];
        });
    });       

    describe('CARDS_NOT_MATCHED', () => {
        const action = {
                type: types.CARDS_NOT_MATCHED,
                data: {id:7}
            };
        let initialState;

        afterEach(() => {
            const newState = cardsReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game empty', () => {
            initialState = [];
        });

        it('when game has started/completed', () => {
            initialState = [{id:9}, {id:8}];
        });
    });    
    
    describe('FIRST_CARD_UP', () => {
        const action = {
                type: types.FIRST_CARD_UP,
                data: {id:7}
            };
        let initialState;

        afterEach(() => {
            const newState = cardsReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game empty', () => {
            initialState = [];
        });

        it('when game has started/completed', () => {
            initialState = [{id:9}, {id:8}];
        });
    });    
       
    describe('IGNORE_CARD', () => {
        const action = {
                type: types.IGNORE_CARD
            };
        let initialState;

        afterEach(() => {
            const newState = cardsReducer(initialState, action);
            expect(newState).toBe(initialState);
        });

        it('when game was not initialized', () => {
            initialState = null;
        });

        it('when game empty', () => {
            initialState = [];
        });

        it('when game has started/completed', () => {
            initialState = [{id:9}, {id:8}];
        });
    }); 

});